\newcommand{\EE}{{S^{\CC}}}
\newcommand{\CC}{{\Z^2}} % set of cells
\newcommand{\La}{\Lambda}
\newcommand{\Lt}{{\tilde \La}}
 %%Coucou de Irene

\section{Introduction}


The purpose of this note is to explore the topic of self-stabilisation in the context of two-dimensional cellular automata. Imagine that we create a bi-dimensional tiling with the constraint that two adjacent tiles have different colours. When this tiling is realised, the artist realises that a) some mistakes occurred during the tiling process and b) the original tiling plan has been lost. The question is to know whether it is possible to correct the tiling to respect the constraints of non-adjacency of colours only by following local rules. 
%In other words, is it possible to go back to the original tiling (or to a new tiling) but changing the colour of each tile according to the colours of the neighbouring tiles only? 
In other words, is it possible to go back to the original tiling (or a possibly different valid tiling) in a finite number of steps, by changing the colour of each tile according to the colours of the neighbouring sites only?
In some sense, the tiling would correct itself almost without an external intervention and we would be allowed of speaking of a self-correction procedure. We here analyse how to implement such procedures with cellular automata: we will assume that all the colours are changed simultaneously according to a local rule.

\section{The model}

We work on infinite grids, the set of cells is denoted by $ \CC $.
%
Let $ k \geq 2 $ be an integer; it will denote the number of colours that will be used. 
The set $ S= \set{0, \dots, k-1} $ represents the different colours (or states) that each cell can hold. 
The state of the cellular automaton at a given time is called a {\em configuration}; the set of configurations is denoted by $ \EE $.

Given two configurations $x,y\in \EE$, we denote the set of cells for which $x$ and $y$ disagree by 
$\Delta(x,y)=\{c\in\CC : x_c \neq y_c\}$.
%
A \emph{finite perturbation} of a configuration $x \in \EE $ is a configuration $ y\in\EE $ such that $\Delta(x,y)$ is finite. 

For a couple of integers $ d \in \CC $, we denote by $ T_d : \EE \to \EE $ the shift by $ d $, that is, the function such that: $ \forall c \in \CC, (T_{d}(x))_c= x_{c-d} $. 

Let $ \La\subset \EE $ denote the set of {\em admissible configurations}; it represents the configurations that one wants to reach after the correction process has occurred. 
We will assume that $ \La $ is invariant by translation, that is, 
$ \forall d \in \CC, x \in \La \implies T_d(x) \in \La$.
   
We denote by  $\Lt$ the set of finite perturbations of the elements of $\La$, that is: $\Lt=\{y\in\EE : \exists x\in \La, |\Delta(x,y)|<\infty\}$. Our goal is to find a cellular automaton that transforms any element of $ \Lt $ into an element of $ \La$.

\bigskip

Let $F:\EE\to \EE$ be a cellular automaton, we say that $ F $ corrects finite perturbations of $\La$ if
\begin{enumerate}%[{\it (i)}]
		\item The configurations of $ \La $ are stable under $ F$: $\forall x\in\La, F(x)=x$, and
		\item the configurations of $ \Lt $ evolve to $ \La$: $\forall y\in\Lt, \exists t\in\N, F^t(y)\in\La$.
\end{enumerate}

{\color{blue} In the case of a probabilistic cellular automaton $\Phi$, we require the two conditions above to be true almost surely, that is
\begin{enumerate}%[{\it (i)}]
		\item $\forall x\in\La, \Phi(x)=x \mbox{ a.s.}$, and
		\item $\forall y\in\Lt, \exists \mbox{ a.s. } t\in\N, \Phi^t(y)\in\La$.
\end{enumerate}}

In this text, we deal with the case where $ \La $ is the $k$-colouring subshift defined by:
$$\La_k=\{x\in\EE : c,c' \in \CC, ||c-c'||_1=1 \implies x_c\not= x_{c'}\}.$$

We show that for $k=2$ and $k\geq 4$, there exist simple rules which correct the $k$-colouring subshift. 
We also discuss the case $k=3$, which is more intricate.  {\color{blue} The $k$-colouring subshift plays the role of a key example, illustrating the difficulty of designing a cellular automaton that self-stabilises on a valid tiling defined by a given set of Wang tiles. For each value of $k$ we consider, we provide some extensions of our methods to other families of subshifts of finite type.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Self-correcting cellular automata}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The case of $2$-colourings}

In this section, $S=\{0,1\}$; $\La_2$ thus contains only two elements, corresponding to the two (odd and even) chequerboard patterns. 

\subsubsection{{\color{blue}Directional correction by a deterministic CA.}}

Let us define a cellular automaton $F$ on $\EE$ by $\forall (i,j) \in \CC,$ 
$$F(x)_{i,j}=
\begin{cases}
1-x_{i,j} &\mbox{ if } x_{i,j}=x_{i+1,j}=x_{i,j+1},\\
x_{i,j} &\mbox{ otherwise. }
\end{cases}
$$

The rule above is similar to the well-known rule of Toom used to correct errors that appear on a uniform background~\cite{Too80}.

%%%%%
\begin{proposition} The cellular automaton $F$ defined above corrects finite perturbations of $\La_2$.
\end{proposition}

\figTwocolouring

\begin{proof} 
It is clear from the definition that $\forall x\in\La_2, F(x)=x$. 

Let $ (T_n)_{n\in_N} $ denote the family of triangular set of cells defined with $ T_n=\{(i,j)\in\CC : i+j\leq n,\; i,j\geq 0\}$ .
Let us take $x\in \La_2$ and $y\in \Lt_2$ {\color{blue} such that $\Delta(x,y)$ is finite}. 
Even if it means translating the configurations, 
we can assume without loss of generality that the differences between the configurations $x$ and $y$ are contained inside a triangle of size $ n $: $ \Delta(x,y) \subset T_n$. 
On the complement of $T_n$, the configuration follows a chequerboard pattern. 
By recurrence, we have: $ \forall n \leq t, \Delta(F^t(y),x) \subset T_{n-t}$, that is, the set of differences gets smaller as time goes by (see Fig.~\ref{fig:twoCol}).
Indeed, the property is initially true by construction. 
Let us assume that for a given $ t $, we have  $ \Delta(F^t(y),x) \subset T_{n-t}$. This implies that the diagonal cells that surround $ T_{n-t} $ North and East are all of the same state, say $ q$. 
Applying the cellular automaton rule to the diagonal of cells of $ T_{n-t} $ will set these cells to the opposite state $ 1- q $, and thus make the colour of these cells coincide with those of $ x $; in other words, the set of differences of $ F^{t+1}(y) $ and $ x $ will be enclosed in $ T_{n-(t+1)}$.  After $ n $ applications of the local rule we have $ \Delta(F^n(y),x)= T_0 = \emptyset $; we thus have $ F^n(y) \in \La_2$, which means that the configuration $ y $ has been corrected.
%Thus, each cell of $T_n^c$ is in a different state from at least either its North or East neighbour. 
%It follows that for all $(i,j)\in T_n^c$, we have 
%$\forall t\in \N, F^t(y)_{i,j}=F^t(x)_{i,j}=x_{i,j}$. 
%Inside $T_n$, the configuration is progressively corrected from the North-East diagonal [corner ?] to the South-West corner. Precisely, after $t\leq n$ steps, we have $F^t(y)_{i,j}=F^t(x)_{i,j}=x_{i,j}$ for all $(i,j)\notin T_{n-t}$, and after $n+1$ steps, the configuration is fully corrected: $T^{n+1}(y)=x\in\tilde\Lambda_2$.
\end{proof}

{\color{blue}
\subsubsection{Isotropic correction by a probabilistic cellular automaton.}

Let $\Neighb$ denote the von Neumann neighbourhood: $\Neighb=\{(0,0),(-1,0),(0,1),(1,0),(0,-1)\}$. We define a probabilistic cellular automaton $\Phi$ on $\EE$ by  $\forall (i,j) \in \CC,$ 
$$\Phi(x)_{i,j}=
\alpha\delta_{\minority{(x_{v} : v\in (i,j)+\Neighb)}}+(1-\alpha)\delta_{x_{i,j}}
$$

\begin{proposition} For $\alpha\in(0,1)$, the probabilistic cellular automaton $\Phi$ defined above corrects finite perturbations of $\La_2$.
\end{proposition}

\begin{proof} Let us take $x\in \La_2$ and $y\in \Lt_2$ such that $\Delta(x,y)$ is finite. Let $R$ be a rectangle such that $\Delta(x,y)\subset R$. For any $(i,j)\notin R$, we have $\Phi(x)_{i,j}=\Phi(y)_{i,j}=x_{i,j}$, so that $\forall t\geq 0, \Delta(\Phi^t(y),x)\subset R$ a.s. Furthermore, inside $R$, $\Phi$ behaves like an absorbing finite state Markov chain, that eventually reaches the chequerboard configuration $(x_{i,j})_{i,j\in R}$. Note that from any state, the chequerboard configuration can be reached in at most $|R|$ steps with positive probability. This is because $\alpha<1$. Otherwise, a monochromatic rectangle could blink between the two states all $0$'s and all $1$'s.
\end{proof}}

{\color{blue}
\subsubsection{Extension to finite SFT.}

The 2-colouring subshift $\La_2$ is a particular case of finite SFT. We now consider an arbitrary finite SFT $\Lambda$ on $S=\{0,\ldots,k-1\}$, for some $k\geq 1$. Then, for any configuration $x\in\Lambda$, there exist integers $m,n\geq 1$ such that $\sigma_h^m(x)=\sigma_h^n(x)=x$, where $\sigma_h$ and $\sigma_v$ denote the vertical and horizontal shift maps. Taking the least common multiple of the collection of integers obtained for the different configurations $x\in\Lambda$ (all these integers are bounded by the cardinal of $\Lambda$), we can find horizontal and vertical periods $p_h, p_v\geq 1$ such that $\forall x\in\Lambda, \sigma_v^{p_v}(x)=\sigma_h^{p_h}(x)=x$. This means that the elements of $\Lambda$ are constant on every sublattice $\LL_{a,b}=\{(a+p_h i, b+p_v j): i,j\in\Z^2\}$.  Therefore, we can simply use Toom's NEC rule on each sublattice, that is, we define a cellular automaton $F$ on $S^{\Z^2}$ by:
$$F(x)_{a,b}=\majority\{x_{a,b},x_{a+p_h,b},x_{a,b+p_v}\},$$
where the majority of three distinct symbols can be taken arbitrarily.}

{\color{red} Is there also a simple isotropic rule?}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The case of $k$-colourings, for $k\geq 5$}

We now consider the case $k\geq 5$. Recall that we have $S=\{0,1,\ldots,k-1\}$. 

\subsubsection{{\color{blue}Directional correction by a deterministic CA.}}

Let us introduce the following terminology. We say that a cell $(i,j)$ has a {\color{blue}NE}-error if it is either in the same state as its {\color{blue}North} neighbour or as its {\color{blue}East} neighbour.
%, that is, if $x_{i,j}=x_{i,j-1}$ or $x_{i,j}=x_{i-1,j}$.
For $x\in S^{\CC}$, we denote by $\Ene(x)$ the set of cells having a {\color{blue}NE}-error, that is
$$\Ene(x)=\{(i,j)\in\CC : x_{i,j}=x_{i+1,j} \mbox{ or } x_{i,j}=x_{i,j+1}\}.$$

Let $ \phi: S^4 \to S $ be a function which assigns to each quadruplet of colours $ (a,b,c,d) $ a colour which is not in the set $ \set{a,b,c,d}$, for example $ \phi(a,b,c,d)= \min S \setminus \set{a,b,c,d} $.
%Let $\phi:S^4\rightarrow S$ be any function (deterministic or probabilistic) such that $\forall (a,b,c,d)\in S^4, \phi(a,b,c,d)\in S\setminus\{a,b,c,d\}$. 
{\color{blue}
We define a cellular automaton {\color{blue} $F$} on $\EE$ by $\forall (i,j) \in \CC$,
$$ F(x)_{i,j}=
\begin{cases}
\phi(x_{i-1,j},x_{i,j-1}, x_{i+1,j},x_{i,j+1}) \mbox{ if } (i,j)\in\Ene \\
x_{i,j} \mbox{ otherwise. }
\end{cases}
$$

%\siamak{I am not sure if I fully understand the role of $\Esw$.  Wouldn't the following work as well?
%\begin{align}
%	\Phi(x)_{i,j} &\isdef
%		\begin{cases}
%			\phi(x_{i-1,j},x_{i,j-1}, x_{i+1,j},x_{i,j+1})
%				& \text{if $(i,j)\in\mathcal{E}_{SW}(x)\setminus\mathcal{E}_N(x)\setminus\mathcal{E}_E(x)$,} \\
%			x_{i,j}	& \text{otherwise.}
%		\end{cases}
%\end{align}
%}
\begin{proposition} Let $k\geq 5$; $F$ defined above corrects finite perturbations of $\La_k$.
\end{proposition}

\begin{proof} It is clear from the definition that $\forall x\in\La_k, F(x)=x$. 
Let us now take $x\in \Lt_k$. 
Even if it means translating the configuration, we can assume without loss of generality that there exists an integer $n\geq 0$ such that $\Ene(x)\subset T_n$. (Recall that $T_n=\{(i,j)\in\CC : i+j\leq n, \; i,j\geq 0\}$).
One can also check that after $k\leq n$ steps, we have $\Ene(F^k(x))\subset T_{n-k}$. Indeed, the set of NE-errors can only decrease under the action of $F$: if $(i,j)\notin \Ene,$ then $(i,j)\notin\Ene(F(x))$, since by definition of $F$, if $(i+1,j)$ or $(i,j+1)$ takes a new color, that new color is different from $x_{i,j}$. Furthermore, if $(i,j)\in\Ene$ is such that $(i+1,j),(i,j+1)\notin\Ene$, then $(i,j)\notin \Ene(F(x))$, so that the set of NE-errors is progressively eroded, from the NE to the SW. After $n+1$ steps, we have: $\Ene(F^{n+1}(x))=\emptyset$, meaning that the configuration is thus fully corrected: $F^{n+1}(x)\in\La_k$. 

%Alternatively, one can prove that the total number of errors strictly decreases at each step, until there are no more errors. Indeed, the definition of the local rule ensures that two consecutive cells cannot be updated at the same time, so that the number of errors can only decrease.
\end{proof}}

{\color{blue}
\subsubsection{Isotropic correction by a probabilistic cellular automaton.}

Let $\phi: S^4 \to S $ be a function as above. For $x\in S^{\CC}$, we denote by $\E(x)$ the set of cells having an error, that is
$$\E(x)=\{(i,j)\in\CC : x_{i,j}\in\{x_{i-1,j},x_{i,j-1}, x_{i+1,j},x_{i,j+1}\}\}$$
We define a probabilistic cellular automaton $\Phi$ on $\EE$ by $\forall (i,j) \in \CC,$
$$ \Phi(x)_{i,j}=
\begin{cases}
\alpha \delta_{\phi(x_{i-1,j},x_{i,j-1}, x_{i+1,j},x_{i,j+1})}+(1-\alpha)\delta_{x_{i,j}} \mbox{ if } (i,j)\in\E(x)\\
\delta_{x_{i,j}} \mbox{ otherwise.}
\end{cases}
$$

\begin{proposition} For $k\geq 5$ and $\alpha\in(0,1)$, the probabilistic cellular automaton $\Phi$ defined above corrects finite perturbations of $\La_k$.
\end{proposition}

\begin{proof} Let $x\in\Lt_k$ be the initial configuration. For any $(i,j)\notin \E(x)$, we have $\Phi(x)_{i,j}=x_{i,j}$, and the neighbouring cells of $(i,j)$ cannot take the state $x_{i,j}$, so that $\E(\Phi(x))\subset \E(x)$ a.s. Furthermore, inside $\E(x)$, $\Phi$ behaves like an absorbing finite state Markov chain, that eventually reaches an allowed configuration. 
\end{proof}}

{\color{blue}
\subsubsection{Extension to single-site fillable SFT.}

A nearest-neighbour SFT is said to be single-site fillable if there exists a map $\phi:\Sigma^4\to \Sigma$, such that for any $(a,b,c,d)\in \Sigma^4$, the pattern defined by: $(x_{1,0},x_{0,1},x_{-1,0},x_{0,-1},x_{0,0})=(a,b,c,d,\phi(a,b,c,d))$ is locally admissible. The two constructions above (directional correction by a deterministic CA, and isotropic correction by a probabilistic cellular automaton) naturally extend to all SFT that are single-site fillable.} {\color{red} More detiails? Reference?}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The case of $4$-colourings}

Let $S=\{0,1,2,3\}$: the case of $4$-colourings is more delicate. 
Obviously, it is no longer possible to use a function $\phi$ with the same properties as above. 
Nevertheless, the next lemma allows us to propose a similar solution.

We set $C_2=\{0,1\}^2$ and we denote by $\partial C_2$ the set of eight direct neighbours of the square $C_2$, that is, $\partial C_2=\{(0,-1),(1,-1),(2,0),(2,1),(1,2),(0,2),(-1,1),(-1,0)\}$.

\begin{lemma}  For any choice of boundary condition $x_{\partial C_2}\in S^{\partial C_2}$, there exists a configuration $x_{C_2}\in S^{C_2}$ that fills the square in such a way that $x_{C_2\cup\partial C_2}$ has no error inside $C_2$, meaning that for all $(i,j)\in C_2$, the point $(i,j)$ is in a different state from all its neighbours.
\label{lem:fourcol}
\end{lemma}



\begin{proof} If we have the possibility to put a same colour on the two points $(0,0)$ and $(1,1)$, or on the two points $(0,1)$ and $(1,0)$, then we are sure that we can find suitable colours for the remaining points.
Indeed, the two other diagonal cells are surrounded by three colours at most and it thus possible to choose the forth colour.
Otherwise, considering the colours on $\partial C_2$, up to a permutation of colours, we can set the colours on $C_2$ with:
$x_{0,0}\in\{1,2\}, x_{1,1}\in\{3,4\}$.

For the two other cells, we are in one of the following cases:
\begin{enumerate}
\item $x_{0,1}\in\{1,2\}, x_{1,0}\in\{3,4\}$,
\item $x_{0,1}\in\{1,3\}, x_{1,0}\in\{2,4\}$.
\end{enumerate}
It can be verified that in the first case, the setting $x_{0,0}=1, x_{1,1}=3$ and $x_{0,1}=2, x_{1,0}=4$ provides a solution and in the second case, a solution is obtained with: $x_{0,0}=1$, $ x_{1,1}=4$, $x_{0,1}=2, x_{1,0}=3$.
\end{proof}

We can now design a CA correcting finite perturbations of $\La_4$. 
Let $\phi:S^{\partial C_2}\rightarrow S^{C_2}$ be a function such that, 
for $x_{\partial C_2}\in S^{\partial C_2}$, returns a pattern $\phi(x)\in S^{C_2}$ which fills the square in an admissible way.
Let us set $C_2(i,j)=\{i,i-1\}\times\{j,j-1\}$. 
The dynamics is as follows: if a cell $(i,j)\in \CC$ has a SW-error and if the cells $(i-1,j+1),(i-1,j+2),(i,j+1),(i,j+2),(i+1,j),(i+1,j+1),(i+1,j+2),(i+2,j-1),(i+2,j),(i+2,j+1)$ have no SW-errors, then the colours on the square $C_2(i,j)$ are replaced by $\phi(x_{\partial C_2(i,j)})$.

\begin{proposition} The CA defined above corrects finite perturbations of $\La_4$.
\end{proposition}


%%% neighb. + "security zone"
\figNeighbFourCol

% \siamak{also $(i+1,j-1)$?}\irene{I would say no! I think if we add it, then if at the beginning, only the two cells $(i,j)$ and $(i-1,j-1)$ \siamak{$(i+1,j-1)$?} have a SW-error, they will never be corrected. I just want to ensure that the first cell to be corrected will be the uppermost/leftmost cell that is on the first diagonal of the kind $\diagdown$ (when scanning these diagonals from the NE to the SW).} \siamak{I see!  I overlooked that.  Does this mean that the correction could take longer than linear time in the size of the smallest triangle encompassing the errors?}

Indeed, it can be verified that the rule ensures that there can be no conflicts for the updates, so that this defines a CA. Furthermore, using the property of Lemma~\ref{lem:fourcol}, the cells are progressively corrected by diagonals, from the North-East to the South-West (and from the North-West to the South-East on each diagonal).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The case of $3$-colourings}

For $k\geq 4$, with the CA above, one can correct the errors in a local way: there exists a radius $R>0$ such that if we observe a finite island of errors, with no errors at a distance smaller than $R$ from it, then we can correct the island without modifying the configuration at a distance larger from $R$ of the island.
However, it is no longer the case for $k=3$, and one has to find another way to treat the problem. 
We will here simply sketch a few remarks on how we could design a solution.
%\siamak{For $k\leq 3$, the $k$-colourings is not strongly irreducible anymore.}

In the lines that follow we will describe the main ingredients to construct such a self-correcting CA.
The idea is to transform a configuration that  is a $3$-colouring into a configuration in the so-called {\em six-vertex model}.
This model is obtained by associating an arrow to each couple of neighbouring cells (horizontal or vertical), these arrows are represented at the boundary between the two cells according the two cells according to following rules.
Let $ q $ and $ q' $ be the colours of the two neighbouring cells. As we have $ q' \neq q$, it follows that we either have $ q'= q + 1 \mod 3 $ or $ q'= q - 1 \mod 3 $.  
\begin{itemize}
\item the vertical boundaries which separate $ q$ and $ q+1$ ($q-1$) have an arrow pointing up (down),
\item the horizontal boundaries which  separate $q$ and $q+1$ ($q-1$) have a right (left) arrow.
\end{itemize}
These conventions are represented on Fig.~\ref{fig:sixVertex}.

\figSixVertex
%\figExampleSV


One can then check that starting from a $3$-colouring, the resulting arrow configuration is such that at each vertex, there are exactly two incoming arrows and two outgoing arrows. Conversely, from a six-vertex configuration, there are three $3$-colourings giving that arrow configuration. (Once we choose the colour of one cell, all the other colours can be deduced). 
%\siamak{This is a very nice representation.  Wouldn't the same representation work for the 2d homomorphism subshift $\Lambda_H$ whenever $H$ is a cycle?}

Figure~\ref{fig:sixVertex} shows an example of such encoding. 
By contrast, Fig.~\ref{fig:SWerror} displays a configuration which holds a finite perturbation of a 3-colouring.
Notice that we have drawn in bold the arrows pointing to the South and the ones pointing to the West. 
%If we consider only those two types of arrows, at each point, we have either one South arrow and a West arrow, or no South arrow and no West arrow. \siamak{or two Souths and no Wests, or no Souths and two Wests, and two Souths and two Wests?} 
In fact, the knowledge of the position of these two types of arrows is sufficient to fully describe the configuration; indeed, the other horizontal or vertical arrows have to be East or North arrows, respectively. 
%So, it is sufficient to can focus on those two types of arrows. 

In the example given, let us imagine that we have fixed the value of a set of cells that form a square, the contour, and that we want to fill the inner part of the square with an admissible configuration. One can verify that the only way to fill the inner part corresponds to a six-vertex configuration that would have a direct South vertical line: indeed, there is only one bold incoming arrow and one bold outgoing arrow in the contour, and we have to connect them. 
So, for every $\ell\geq 1$, we can find a configuration which is a square $\ell\times\ell$ which contains only {\em one} error at the centre, but for which it is necessary to modify the configuration at a distance $\ell$ from the error in order to recover an admissible configuration.
%\siamak{Makes sense.  I guess they use a similar argument to show $\Lambda_3^2$ is not strongly irreducible?}
This observation leads us to think that it will be difficult to find a solution without using additional states.

\figSVcaseError

If such additional states are used, our goal would be to ``connect the arrows'' in a local way. We are currently looking for such a construction.


\section{Discussion}

We presented a first analysis on the possibility to use self-correcting cellular automata that operate on the $k$-colouring subshift. The cases $ k= 2$ and $ k \geq 5$ are the most easy ones; the case $ k=4 $ can be dealt with rather easily; however, the case $ k =3 $ is much more delicate.

The case of $k$-colourings presented in this note should be considered as a first step towards a wider view of the process of self-correcting cellular automata. A logical extension of the framework is to consider subshifts of finite type (SFT). Let us briefly discuss this direction of research.

In the case of a {\em finite} SFT, the method exposed for the binary case $ k = 2 $ can be readily extended. Indeed, such an SFT is necessarily spatially periodic: there exist two integers  $m,n\geq 1$ such that for any configuration $ x$, $\sigma_\mathrm{h}^m(x)=\sigma_\mathrm{v}^n(x)=x$, where $\sigma_\mathrm{h}$ and $\sigma_\mathrm{v}$ denote the vertical and horizontal shift maps. We can then simply use Toom's NEC rule on each sublattice, that is, $(Fx)_{a,b}\isdef\maj\{x_{a,b},x_{a+m,b},x_{a,b+n}\}$, where the majority of three distinct symbols can be taken arbitrarily~\cite{Too80}.

Similar techniques can also be applied to correct South-West deterministic tilings. 
In this case, additional states -- which can be part of the perturbation -- and signals can be used in order to ensure that the corrections are applied without introducing new errors\footnote{We plan to expose this point in an extended version of this article. The one-dimensional case has also been studied by T\"orm\"a (unpublished).}.

In the case where $ k \geq 4$, we think that our constructions can be generalised to the case of nearest-neighbour SFT that are {\em single-site fillable}~\cite{Bri17}: if there exists a map $\phi:S^4\to S$, such that for any $(a,b,c,d)\in S^4$, the pattern defined by: $(x_{1,0},x_{0,1},x_{-1,0},x_{0,-1},x_{0,0})=(a,b,c,d,\phi(a,b,c,d))$ is locally admissible, then the techniques described above should also apply.

All the solutions discussed so far have the advantage to operate in a time that is linear in the number of errors. However, finding such rules is not always possible.
In the case where no adapted method can be applied, it is still possible to use a kind of ``brute-force'' method : each error would be the centre of a self-correcting zone. The cellular automaton should then test sequentially if there are admissible solutions inside this zone. If the answer is positive, then the part is corrected, if not, then the zone is extended by one cell in each direction. When two such zones meet, there should be some procedures to control the correct merging of the zones and the correct ``restart'' of the process. It is clear that even though each step can be thought of separately in a clear way, putting all the steps together in a cellular automaton that effectively works is a huge task. Moreover, the time needed for such a rule to operate would be more than exponential in the number of errors.

The readers might also have noticed that our solutions are strongly anisotropic: the corrections are applied in a given order, according to some particular directions. It is an interesting question to know whether it is possible to find solutions defined with isotropic rules. In this directions of research, we believe that stochastic rules could be of particular interest.      


